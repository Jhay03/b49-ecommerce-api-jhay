const express = require("express")
const router = express.Router()
const Product = require("./../models/Product")
const Transaction = require("./../models/Transaction")
const User = require("./../models/User")
const isAdmin = require("../auth")
const jwt = require("jsonwebtoken")
const stripe = require('stripe')('sk_test_1e06Kd7efHuWpXL2uZrlImbY00lIjcpnra')

//add transaction POST http://localhost:4000/transactions
router.post("/", (req, res) => {
    //console.log(req.body)
    let token = req.headers['x-auth-token'];
    if (!token) return res.status(401).json({ message: "You are not logged in.", status: 401 })
    
    let decoded = jwt.verify(token, 'b49-ecommerce')
    if (decoded) {
        let transaction = new Transaction()
        transaction.userId = decoded._id
        transaction.paymentMode = "COD"
        transaction.total = req.body.total
        transaction.products = req.body.orders
        //console.log(transaction)
        transaction.save()
        return res.json({status: 200, message: "Transaction Successful"})
    }
})


//localhost:4000/stripes
router.post("/stripe", (req, res) => {
    //console.log(req.body)
    let token = req.headers['x-auth-token'];
    if (!token) return res.status(401).json({ message: "You are not logged in.", status: 401 })
    
    let decoded = jwt.verify(token, 'b49-ecommerce')
    if (decoded) {
        let transaction = new Transaction()
        transaction.userId = decoded._id
        transaction.paymentMode = "Stripe"
        transaction.total = req.body.amount
        transaction.products = req.body.cartItems
       // transaction.save()

     
        let body = {
            source: req.body.token.id,
            amount: req.body.amount,
            email: req.body.email,
            currency: "PHP"
        }

        stripe.charges.create(body, (err, result) => {
            if (result) {
                transaction.save()
                return res.json({status:200, message:"Transaction Successful"})
            } else {
                return res.send(err)
            }
        })

    }
})

//get all transactions
router.get("/", isAdmin, (req, res) => {
    Transaction.find({}, (err, transactions) => {
        return res.json(transactions)
    })
})

//edit all transactions by the admin
router.put("/:id", isAdmin, (req, res) => {
    Transaction.findOne({_id: req.params.id }, (err, transaction) => {
        transaction.statusId = req.body.statusId
        transaction.save()
        return res.json({status: 200, message: "Status Changed!"})
    })
})

//get all transaction by the user
//localhost:4000/transactions/userId
router.get("/:userId", (req,res) => {
    let token = req.headers['x-auth-token']
    if (!token) return res.json({ status: 401, message: "You are not logged in!" })
    {/*, (err, transactions) =>*/}
    Transaction.find({ userId: req.params.userId })
    .then(transaction => res.json(transaction))
})
module.exports = router;