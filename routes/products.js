const express = require("express")
const router = express.Router()
const Product = require("../models/Product")
const isAdmin = require("../auth")
const multer = require("multer")


let storage = multer.diskStorage({
    destination: function (req, file, cb){
       cb(null, 'public/products')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})

let upload = multer({ storage: storage })

//add a product POST localhost:4000/products
router.post("/",upload.single('image'), isAdmin, (req, res) => {
    let product = new Product()
    product.name = req.body.name
    product.description = req.body.description
    product.price = req.body.price
    product.image = "/products/"+req.file.filename
    product.categoryId = req.body.categoryId
    product.save()
    return res.status(200).json({product, message: "Product Added Successfully!", status:200})
})

//view all products get localhost:4000/products
router.get('/', (req, res) => {
    Product.find({}, (err, products) => {
        return res.json(products)
    })
})

//view single product by id localhost:4000/products/id
router.get("/:id", (req, res) => {
    Product.findOne({ _id: req.params.id }, (err, product) => {
        return res.json(product)
    })
})

//edit product localhost:4000/products/id
router.put("/:id", upload.single('image'), isAdmin, (req, res) => {
    let updatedProduct = { ...req.body }
    if (req.file) {
        updatedProduct = {...req.body, image: "/products/" + req.file.filename}
    }
    Product.findOneAndUpdate(
        { _id: req.params.id },
        updatedProduct,
        { new: true },
        (err, updated) => {
            return res.status(200).json({updated, message: "Product updated Successfully", status: 200})
        }
    )

})

//delete product localhost:4000/products/id
router.delete("/:id", isAdmin, (req, res) => {
    Product.findOneAndDelete({ _id: req.params.id }, (err, product) => {
        return res.status(200).json({product, message: "Product deleted successfully", status: 200})
    })
})
module.exports = router