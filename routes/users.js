const express = require("express")
const router = express.Router()
const User = require("../models/User")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")

//register
router.post("/", (req, res) => {
    //username must be greater than 8 characters
    if (req.body.username.length < 8) return res.status(400).json({
        status: 400,
        message: "Username must be greater than 8 characters"
    })
    //password must be greater than 8 characters
    if (req.body.password.length < 8) return res.status(400).json({
        status: 400,
        message: "Password must be greater than 8 characters"
    })
    //password2 must be greater than 8 characters
    if (req.body.password2.length < 8) return res.status(400).json({
        status: 400,
        message: "Password2 must be greater than 8 characters"
    })    
    //password should match
    if (req.body.password != req.body.password2) return res.status(400).json({
        status: 400,
        message: "Password doesn't match!"
    })
    //check if username is already exist.
    User.findOne({ username: req.body.username }, (err, user) => {
        if (user) return res.status(400).json({
            status: 400,
            message: "Username already exists!"
        })
        bcrypt.hash(req.body.password, 10, (err, hashedPassword) => {
            const user = new User()
            user.fullname = req.body.fullname
            user.username = req.body.username
            user.password = hashedPassword
            user.save()
            return res.json({
                status: 200,
                message: "Registered Successfully!"
            })
        })
    })
})

//login  POST LOCALHOST:4000/users/login
router.post("/login", (req, res) => {
    User.findOne({ username: req.body.username }, (err, user) => {
        if (err || !user) return res.status(400).json({
            status: 400,
            message: "No user found"
        })
        bcrypt.compare(req.body.password, user.password, (err, result) => {
            if (!result) {
                return res.status(401).json({
                    auth: false,
                    message: "Invalid Credentials",
                    token: null
                })
            } else {
                user = user.toObject()
                delete user.password
                let token = jwt.sign(user, 'b49-ecommerce', { expiresIn: '1h' })
                return res.status(200).json({
                    auth: true,
                    message: "Logged in Successfully",
                    user,
                    token
                })
            }
        })
    })
})
module.exports = router;