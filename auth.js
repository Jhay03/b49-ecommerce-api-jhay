const jwt = require("jsonwebtoken")
const User = require("./models/User")


const isAdmin = (req, res, next) => {
    let token = req.headers["x-auth-token"]
    if(!token) return res.status(401).json({message: "Not Logged In"})
    jwt.verify(token, 'b49-ecommerce', (err, decoded) => {
        if (decoded.isAdmin === false) return res.status(401).json({
            status: 401,
            message: "Unauthorized!"
        })
        next()
    })
}

module.exports = isAdmin;