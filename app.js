const express = require("express") //creating server using java script
const app = express() // store in app
const PORT = process.env.PORT || 4000 //if hosted statement 1 else statement 2
const cors = require("cors") // package to connect frotn end and back end
const mongoose = require("mongoose") // to connect on database

//mongoose connection
mongoose.connect("mongodb://localhost/b49-ecommerce", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

const db = mongoose.connection
db.once('open', () => console.log("You are now connected to MongoDB"))

app.use(cors())
app.use(express.json())
app.use(express.static('public'))

//define routes/resources
app.use("/users", require("./routes/users"))
app.use("/products", require("./routes/products"))
app.use("/categories", require("./routes/categories"))
app.use("/transactions", require("./routes/transactions"))

app.listen(PORT, () => console.log(`Server is running on port: ${PORT}`))

